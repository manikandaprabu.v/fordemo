package demoMaven.TestCases;

import java.time.Duration;
import static demoMaven.Reporting.ExtentTestManager.startTest;
import static demoMaven.Reporting.ExtentTestManager.infoLogs;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringDecorator;
import org.openqa.selenium.support.events.WebDriverListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import demoMaven.Reporting.ExtentTestManager;
import demoMaven.Utility.ReadElementInfo;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Sample {
	
	static WebDriver driver;
	WebDriverWait wait;
	
		
	@Test
	public void doLogin()
	{
		startTest("Login", "Verify Login");
		driver=BaseTest.getDriver();
		wait= new WebDriverWait(driver, Duration.ofSeconds(10));
		SoftAssert assertion = new SoftAssert();
		infoLogs("Validating the title");
		wait.until(ExpectedConditions.titleIs(ReadElementInfo.readElementData("homePageTitle")));
		ReadElementInfo.getElement("username", driver).sendKeys("Admin");
		//driver.findElement(By.id("txtUsername")).sendKeys("Admin");
		ReadElementInfo.getElement("password", driver).sendKeys("admin123");
		//driver.findElement(By.id("txtPassword")).sendKeys("admin123");
		ReadElementInfo.getElement("loginbutton", driver).click();
		//driver.findElement(By.id("btnLogin")).click();
		assertion.assertTrue(driver.getCurrentUrl().contains(ReadElementInfo.readElementData("dashboardURL")));
		ReadElementInfo.getElement("welcomemessage", driver).click();
		//driver.findElement(By.id("welcome")).click();
		ReadElementInfo.getElement("logoutlink", driver).click();
		//driver.findElement(By.linkText("Logout")).click();
		assertion.assertTrue(driver.getCurrentUrl().contains(ReadElementInfo.readElementData("LoginPageURL")));
		assertion.assertAll();
	}


	public static WebDriver getDriver() {
		// TODO Auto-generated method stub
		return driver;
	}
	

}
