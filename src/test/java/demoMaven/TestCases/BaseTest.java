package demoMaven.TestCases;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest {
	
	static WebDriver eventDriver ;
	WebDriverWait wait;
	
	@BeforeTest
	public void initBrowser()
	{
		WebDriverManager.chromedriver().setup();
		eventDriver = new ChromeDriver();
		wait = new WebDriverWait(eventDriver, Duration.ofSeconds(10));
		eventDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		eventDriver.manage().window().maximize();
		eventDriver.navigate().to("https://opensource-demo.orangehrmlive.com/");
	}
	
	@AfterTest
	public void tearDown()
	{
		eventDriver.close();
	}
	

	public static WebDriver getDriver() {
		return eventDriver;
	}
	

}
