package demoMaven.Reporting;

import java.util.HashMap;
import java.util.Map;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class ExtentTestManager {
	
	static Map<Integer,ExtentTest> extentTestMap = new HashMap<>();
	static ExtentReports extent =ExtentManager.createExtentReports();
	static ExtentTest test;
	
	public static ExtentTest getTest() {
		return extentTestMap.get((int)Thread.currentThread().getId());
	}
	
	public static ExtentTest startTest(String testName,String testDesc)
	{
		test = extent.createTest(testName,testDesc);
		extentTestMap.put((int)Thread.currentThread().getId(), test);
		return test;
	}
	
	public static ExtentTest infoLogs(String desc)
	{
		return test.log(Status.INFO, desc);
	}

}
