package demoMaven.Reporting;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentManager {
	
	public static final ExtentReports extentReports = new ExtentReports();
	
	public static ExtentReports createExtentReports()
	{
		ExtentSparkReporter reporter = new ExtentSparkReporter("./target/extent-reports.html");
		reporter.config().setReportName("Regression Suite");
		extentReports.attachReporter(reporter);
		extentReports.setSystemInfo("For Intellipat", "Demo");
		extentReports.setSystemInfo("Author", "Vijay");
		return extentReports;
		
	}

}
