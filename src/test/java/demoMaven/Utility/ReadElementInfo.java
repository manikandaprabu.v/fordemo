package demoMaven.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ReadElementInfo {
	
	public static String readElementData(String elementName)
	{
		FileInputStream fis;
		Properties prop = null;
		try {
			System.out.println(System.getProperty("user.dir")+"/src/test/resources/elementInfo.properties");
			fis = new FileInputStream(new File(System.getProperty("user.dir")+"/src/test/resources/elementInfo.properties"));
			prop = new Properties();
			prop.load(fis);
			System.out.println("Data==>"+prop.getProperty(elementName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return prop.getProperty(elementName);
		
	}
	
	public static WebElement getElement(String elemName,WebDriver driver)
	{
		WebElement ele=null;
		String [] elementDetails = readElementData(elemName).split(":");
		switch(elementDetails[0])
				{
		case "id":
			ele = driver.findElement(By.id(elementDetails[1]));
			break;
		case "name":
			ele = driver.findElement(By.name(elementDetails[1]));
			break;
		case "xpath":
			ele = driver.findElement(By.xpath(elementDetails[1]));
			break;
		case "linktext":
			ele = driver.findElement(By.linkText(elementDetails[1]));
			break;
				}
		return ele;
		
	}

}
