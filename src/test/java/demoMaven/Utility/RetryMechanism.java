package demoMaven.Utility;

import java.util.concurrent.CountDownLatch;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryMechanism implements IRetryAnalyzer {

	private int count=0;
	private static int maxTry=2;
	
	@Override
	public boolean retry(ITestResult result) {
		if(!result.isSuccess())
		{
				if(count<maxTry)
				{
					count++;
					result.setStatus(result.FAILURE);
					return true;
				}
		}
		else {
			result.setStatus(result.SUCCESS);
		}
		return false;
	}
	
	

}
