package demoMaven.Listeners;

import java.util.Objects;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import static demoMaven.Reporting.ExtentTestManager.getTest;
import com.aventstack.extentreports.Status;
import demoMaven.Reporting.ExtentManager;
import demoMaven.TestCases.BaseTest;
import demoMaven.TestCases.Sample;

public class TestListener extends BaseTest implements ITestListener {
	
	private static String getMethodName(ITestResult iTestResult)
	{
		return iTestResult.getMethod().getConstructorOrMethod().getName();
	}
	
	@Override
	public void onStart(ITestContext context) {
		System.out.println("Im on the starting method" +context.getName());
	        context.setAttribute("WebDriver", BaseTest.getDriver());
	        
	}
	
	@Override
	public void onFinish(ITestContext context) {
		System.out.println("Im on the finishing method" +context.getName());
			ExtentManager.extentReports.flush();
	}
	
	@Override
	public void onTestStart(ITestResult result) {
		System.out.println(getMethodName(result) + "test started");
	}
	
	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println(getMethodName(result) + "test success");
		getTest().log(Status.PASS, "Test Passed");
		
	}
	
	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println(getMethodName(result)+"Failed");
		Object testClass= result.getInstance();
		WebDriver driver =((Sample)testClass).getDriver();
		String screenshot ="data:image/png;base64,"+((TakesScreenshot)Objects.requireNonNull(driver)).getScreenshotAs(OutputType.BASE64);
		getTest().addScreenCaptureFromBase64String(screenshot).getModel().getMedia().get(0);
		getTest().log(Status.FAIL, "Test Failed");
				}
	
	

}
